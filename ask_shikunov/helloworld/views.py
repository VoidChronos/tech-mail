from django.shortcuts import render
from django.http import HttpResponse

def helloworld(request):
    html = "<html><body>"
    for k,v in request.REQUEST.items():
        html += "<br>" + str(k) + "  =  " + str(v) + "</br>"
    html+="Hello World</body></html>"
    return HttpResponse(html)
