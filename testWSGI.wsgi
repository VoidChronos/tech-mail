from cgi import parse_qs

def application(environ, start_response):
	output = "Hello World"
	for key, value in environ.items():
		if (str(key).startswith("HTTP")):
			output += "\n" + str(key) + " = " + str(value)
		if (str(key) == "QUERY_STRING"):
			output += "\n" + str(key) + " = "
			for val in value.split("&"):
				output += val + " | "
	try:
		request_body_size = int(environ.get('CONTENT_LENGTH', 0))
	except(ValueError):
		request_body_size = 0

	request_body = environ['wsgi.input'].read(request_body_size)
	d = parse_qs(request_body)
	for key,value in d.items():
		output += "\n" + str(key) + "  =  " + str(value)
	start_response('200 OK', [('content-type', 'text/plain')])
	return [output]
